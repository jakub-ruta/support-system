-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 01. čen 2021, 21:28
-- Verze serveru: 10.4.17-MariaDB
-- Verze PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `support.topescape.local`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `support_action_log`
--

CREATE TABLE `support_action_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `IP` varchar(32) DEFAULT NULL,
  `Browser` varchar(128) DEFAULT NULL,
  `sess_id` int(11) DEFAULT NULL,
  `method` varchar(6) DEFAULT NULL,
  `event_name` varchar(16) DEFAULT NULL,
  `event_type` varchar(16) DEFAULT NULL,
  `event_action` varchar(16) DEFAULT NULL,
  `session_type` set('normal','admin','cron','webhooks','unknown') NOT NULL DEFAULT 'unknown',
  `aditional_data` text DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `page` varchar(256) DEFAULT NULL,
  `TTL` int(11) NOT NULL DEFAULT 1000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_annoucements`
--

CREATE TABLE `support_annoucements` (
  `annoucement_id` int(11) NOT NULL,
  `annoucement_name` varchar(32) NOT NULL,
  `annoucement_text` text NOT NULL,
  `annoucement_type` varchar(16) NOT NULL,
  `annoucement_prevent_actions` tinyint(1) NOT NULL DEFAULT 0,
  `annoucement_created` datetime NOT NULL DEFAULT current_timestamp(),
  `annoucement_owner` int(11) NOT NULL,
  `annoucement_display_owner` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_annoucement_assign`
--

CREATE TABLE `support_annoucement_assign` (
  `log_id` int(11) NOT NULL,
  `annoucement_id` int(11) NOT NULL,
  `annoucement_status` varchar(8) NOT NULL DEFAULT 'new',
  `annoucement_user_id` int(11) NOT NULL,
  `annoucement_update_time` datetime DEFAULT NULL,
  `annoucement_due` datetime NOT NULL DEFAULT current_timestamp(),
  `annoucement_expiry` datetime NOT NULL DEFAULT '9999-01-01 00:00:00',
  `annoucement_add_data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_cron_tasks_overview`
--

CREATE TABLE `support_cron_tasks_overview` (
  `task_id` int(11) NOT NULL,
  `task_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `task_enabled` tinyint(1) NOT NULL DEFAULT 1,
  `task_default_priority` tinyint(4) NOT NULL DEFAULT 5,
  `task_repeat` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `task_default_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `task_path` varchar(128) COLLATE utf8_bin NOT NULL,
  `task_long` tinyint(4) NOT NULL DEFAULT 2,
  `task_default_due` int(11) NOT NULL DEFAULT 0,
  `task_default_expiry` int(11) NOT NULL DEFAULT 5184000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Vypisuji data pro tabulku `support_cron_tasks_overview`
--

INSERT INTO `support_cron_tasks_overview` (`task_id`, `task_name`, `task_enabled`, `task_default_priority`, `task_repeat`, `task_default_data`, `task_path`, `task_long`, `task_default_due`, `task_default_expiry`) VALUES
(1, 'Task dispatch', 1, 0, '12 hours', NULL, 'this/tasks_dispatch', 1, 0, 580000),
(2, 'Repair - Uncomplete tasks handler', 1, 0, '1 hour', NULL, 'this/handleExpiredTasks/', 0, 0, 3600),
(3, 'Gen time - archive', 1, 2, '24 hours', NULL, 'this/archive/gen_time', 2, 0, 83000),
(4, 'Action log - delete old', 1, 2, '24 hours', NULL, 'this/archive/action_log', 2, 0, 83000),
(5, 'Settings handler (auto update)', 1, 1, '15 minutes', NULL, 'this/updateSettings', 1, 0, 850),
(26, 'Tickets - close and archive', 1, 5, '12 hours', NULL, 'TicketsUtils/autoCloseTickets', 1, 0, 850),
(27, 'Tickets - unasigne non answered', 1, 7, '2 hours', NULL, 'TicketsUtils/autoUnasigne', 1, 0, 850),
(28, 'Tickets - unsnooze', 1, 7, '5 minutes', NULL, 'TicketsUtils/autoUnsnooze', 1, 0, 60);

-- --------------------------------------------------------

--
-- Struktura tabulky `support_cron_tasks_queue`
--

CREATE TABLE `support_cron_tasks_queue` (
  `queue_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `task_priority` tinyint(4) NOT NULL DEFAULT 5,
  `task_created` datetime NOT NULL DEFAULT current_timestamp(),
  `task_due` datetime NOT NULL DEFAULT current_timestamp(),
  `task_expiry` datetime DEFAULT NULL,
  `task_owner` int(11) NOT NULL,
  `task_status` int(11) NOT NULL DEFAULT 0 COMMENT '0-new;1-running;2-completed;3-completed_with_error;4-aborted',
  `task_completed` datetime DEFAULT NULL,
  `task_last_change` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `task_data` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_departments`
--

CREATE TABLE `support_departments` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `department_code` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_groups`
--

CREATE TABLE `support_groups` (
  `group_id` int(11) NOT NULL,
  `group_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `group_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_internal_remote_token`
--

CREATE TABLE `support_internal_remote_token` (
  `id` int(11) NOT NULL,
  `function` set('get','set','admin','setup','send') NOT NULL,
  `app_code` varchar(64) NOT NULL,
  `token` varchar(256) NOT NULL,
  `update_time` datetime NOT NULL DEFAULT '1000-01-01 00:00:00' ON UPDATE current_timestamp(),
  `support_auto_update` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_ip_bans`
--

CREATE TABLE `support_ip_bans` (
  `id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `deny` tinyint(1) NOT NULL,
  `allow_write_ticket` tinyint(4) NOT NULL,
  `internal_note` text DEFAULT NULL,
  `visible_note` text DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 1,
  `time_create` datetime NOT NULL DEFAULT current_timestamp(),
  `ban_expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_issues`
--

CREATE TABLE `support_issues` (
  `issue_id` int(11) NOT NULL,
  `issue_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT 'change me!',
  `res_id` int(11) NOT NULL,
  `issue_created` datetime NOT NULL DEFAULT current_timestamp(),
  `issue_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `issue_enabled_from` datetime NOT NULL DEFAULT current_timestamp(),
  `issue_enable_to` datetime NOT NULL DEFAULT '2054-01-01 00:00:00',
  `issues_types` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`issues_types`)),
  `issue_created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_issues_text`
--

CREATE TABLE `support_issues_text` (
  `text_id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `issue_text` text COLLATE utf8_bin NOT NULL,
  `issue_lang` varchar(8) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_resources`
--

CREATE TABLE `support_resources` (
  `res_id` int(11) NOT NULL,
  `res_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `res_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `res_public` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_resources_in_department`
--

CREATE TABLE `support_resources_in_department` (
  `log_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `dep_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_resources_in_groups`
--

CREATE TABLE `support_resources_in_groups` (
  `log_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_sessions`
--

CREATE TABLE `support_sessions` (
  `sess_id` int(11) NOT NULL,
  `sess_token` varchar(128) NOT NULL,
  `sess_user_internal_id` int(11) NOT NULL,
  `sess_expire` int(11) NOT NULL,
  `sess_created` datetime NOT NULL DEFAULT current_timestamp(),
  `valid` tinyint(1) NOT NULL DEFAULT 1,
  `sess_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_settings`
--

CREATE TABLE `support_settings` (
  `sett_id` int(11) NOT NULL,
  `sett_key` varchar(32) NOT NULL,
  `sett_category` varchar(32) NOT NULL,
  `sett_value` varchar(128) NOT NULL DEFAULT '0',
  `sett_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sett_time_to_update` datetime DEFAULT '1000-01-01 00:00:00',
  `sett_change_to_when_update` varchar(128) NOT NULL DEFAULT 'null ',
  `sett_default` varchar(32) NOT NULL DEFAULT '0',
  `sett_type` set('bool','uint','int','float','string') NOT NULL DEFAULT 'bool',
  `sett_visible_name` varchar(128) NOT NULL,
  `sett_required_level` tinyint(3) UNSIGNED NOT NULL DEFAULT 4
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `support_settings`
--

INSERT INTO `support_settings` (`sett_id`, `sett_key`, `sett_category`, `sett_value`, `sett_updated`, `sett_time_to_update`, `sett_change_to_when_update`, `sett_default`, `sett_type`, `sett_visible_name`, `sett_required_level`) VALUES
(1, 'enable_rights_synchronizations', 'LDAP', '0', '2021-02-11 10:14:54', '2020-11-13 11:05:25', ' n', '1', 'bool', 'Enable users and rights synchronization with LDAP (must be off always on portal)', 5),
(2, 'enable_app_login', 'SYSTEM', '0', '2021-04-08 12:35:04', '2020-11-13 11:05:28', 'n', '1', 'bool', 'Enable login to this app (admin level 3 and higher can log in always)', 3),
(3, 'enable_PUSH', 'SYSTEM', '1', '2020-11-19 13:38:42', '2020-11-13 11:05:30', 'n', '1', 'bool', 'Enable if push notifications can be send from this app (if disabled PUSH notifications will be discarded)', 4),
(4, 'enable_EMAIL', 'SYSTEM', '0', '2020-11-13 11:12:47', '2020-11-13 11:05:31', 'n', '1', 'bool', 'Enable if emails can be send from this app (if disabled mails will be stored in queue and will be dispatched when enable)', 4),
(5, 'app_visible_name', 'SYSTEM', 'Support tool', '2021-04-08 12:35:23', '2020-11-13 11:05:33', 'n', 'Support tool', 'string', 'Set app visible name', 3),
(6, 'maitenance_mode', 'APP', '1', '2021-02-11 10:12:43', '0000-00-00 00:00:00', ' ', '0', 'bool', 'Enable means require portal login to see all public pages (should be off always)', 3),
(7, 'landing_page', 'SYSTEM', 'dashBoard', '2021-02-04 15:44:53', '2020-11-13 11:05:33', 'n', 'dashBoard', 'string', 'Set main page', 4),
(8, 'enable_CRONS', 'SYSTEM', '1', '2021-02-27 17:00:54', '2020-11-13 11:05:31', 'n', '1', 'bool', 'Enable crons jobs. Be carefull when switch off... (Note: Some cron jobs are not dependent on this settings) (should be always on', 3),
(9, 'local_login', 'SYSTEM', '0', '2021-05-18 21:54:22', '2020-11-13 11:05:30', 'n', '1', 'bool', 'Enable if local logins should be enabled.', 4),
(10, 'google_login', 'SYSTEM', '0', '2021-02-04 15:57:43', '2020-11-13 11:05:30', 'n', '0', 'bool', 'Enable if google logins should be enabled.', 4),
(11, 'ldap_login', 'SYSTEM', '1', '2021-04-08 12:35:39', '2020-11-13 11:05:30', 'n', '1', 'bool', 'Enable if ldap logins should be enabled.', 4),
(12, 'can_register', 'SYSTEM', '0', '2021-05-18 20:38:33', '0000-00-00 00:00:00', 'null ', '0', 'bool', 'If new account can be localy registered (if off only system can create new users)', 4),
(13, 'app_header_text', 'APP', 'Local server!', '2021-04-29 21:06:26', '1000-01-01 00:00:00', 'n', 'null', 'string', 'Text for special information (f.e. for maitenance) Vissible everywhere! (should be used only by devs)', 2),
(14, 'logo', 'SYSTEM', '0', '2021-05-31 12:54:13', '1000-01-01 00:00:00', 'null ', '0', 'bool', '', 4);

-- --------------------------------------------------------

--
-- Struktura tabulky `support_supporters_signatures`
--

CREATE TABLE `support_supporters_signatures` (
  `sig_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `res_id` int(11) NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `sig_updated` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_tickets`
--

CREATE TABLE `support_tickets` (
  `ticket_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT 1,
  `ticket_subject` varchar(64) COLLATE utf8_bin NOT NULL,
  `ticket_level` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `ticket_status` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT 'open',
  `ticket_allow_rating` tinyint(1) NOT NULL DEFAULT 1,
  `ticket_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `ticket_first_post_time` datetime NOT NULL DEFAULT current_timestamp(),
  `ticket_last_post_time` datetime NOT NULL DEFAULT current_timestamp(),
  `ticket_open_post_time` datetime NOT NULL DEFAULT current_timestamp(),
  `ticket_origin` varchar(8) COLLATE utf8_bin NOT NULL DEFAULT 'user',
  `ticket_snooze_until` datetime DEFAULT NULL,
  `ticket_can_be_closed_by_user` tinyint(1) NOT NULL DEFAULT 1,
  `ticket_add_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_tickets_messages`
--

CREATE TABLE `support_tickets_messages` (
  `msg_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `msg_ip` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `msg_browser` text COLLATE utf8_bin DEFAULT NULL,
  `msg_text` text COLLATE utf8_bin NOT NULL,
  `msg_type` set('user','admin','comment','') COLLATE utf8_bin NOT NULL DEFAULT 'user',
  `msg_admin_id` int(11) DEFAULT NULL,
  `msg_created` datetime NOT NULL DEFAULT current_timestamp(),
  `msg_PII` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_tickets_messages_who_saw`
--

CREATE TABLE `support_tickets_messages_who_saw` (
  `record_id` int(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `who_time` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_tokens`
--

CREATE TABLE `support_tokens` (
  `token_id` int(11) NOT NULL,
  `user_internal_id` int(11) DEFAULT NULL,
  `token_value` varchar(128) COLLATE utf8_bin NOT NULL,
  `token_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `token_created` datetime NOT NULL DEFAULT current_timestamp(),
  `token_expire` datetime NOT NULL,
  `token_updated` datetime NOT NULL DEFAULT '1000-01-01 00:00:00' ON UPDATE current_timestamp(),
  `token_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_types`
--

CREATE TABLE `support_types` (
  `type_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `type_name` varchar(32) COLLATE utf8_bin NOT NULL,
  `type_vissible` tinyint(1) NOT NULL DEFAULT 1,
  `type_code` varchar(16) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_users_creditals`
--

CREATE TABLE `support_users_creditals` (
  `user_internal_id` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `salt1` varchar(256) COLLATE utf8_czech_ci NOT NULL DEFAULT 't&#ssdf54gh',
  `salt2` varchar(256) COLLATE utf8_czech_ci NOT NULL DEFAULT 'ewtrztbunectzv89465132vuhb',
  `password_type` varchar(30) COLLATE utf8_czech_ci NOT NULL DEFAULT 'old_hash',
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_users_data`
--

CREATE TABLE `support_users_data` (
  `user_internal_id` int(11) NOT NULL,
  `real_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `address` text COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `company` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `gender` set('male','female','none','') COLLATE utf8_bin NOT NULL DEFAULT 'none'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_users_in_departments`
--

CREATE TABLE `support_users_in_departments` (
  `id` int(11) NOT NULL,
  `user_internal_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_role` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_users_personal_informations`
--

CREATE TABLE `support_users_personal_informations` (
  `user_internal_id` int(11) NOT NULL,
  `user_portal_id` int(11) DEFAULT NULL,
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nickname` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `mail` varchar(128) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `admin_level` tinyint(4) NOT NULL DEFAULT 0,
  `timestamp_registered` timestamp NOT NULL DEFAULT current_timestamp(),
  `disabled_account` tinyint(1) NOT NULL DEFAULT 0,
  `origin` varchar(16) NOT NULL DEFAULT 'local',
  `locale` varchar(3) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL DEFAULT 'cs',
  `avatar` varchar(125) NOT NULL DEFAULT 'https://cdn2.agilitysalt.eu/g/CZ_topescape_LDAP/240px-No_image_available.svg.png',
  `timestamp_updated` datetime DEFAULT NULL,
  `timestamp_last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_user_rules`
--

CREATE TABLE `support_user_rules` (
  `id` int(11) NOT NULL,
  `user_internal_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL DEFAULT 0,
  `valid_from` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `valid_to` datetime NOT NULL DEFAULT '2999-01-01 00:00:00',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `support_user_rules_list`
--

CREATE TABLE `support_user_rules_list` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `sub_right` varchar(16) NOT NULL DEFAULT 'edit',
  `visible_name` varchar(48) NOT NULL DEFAULT '''''',
  `description` text DEFAULT '',
  `granteable` tinyint(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `support_action_log`
--
ALTER TABLE `support_action_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `IP` (`IP`),
  ADD KEY `method` (`method`),
  ADD KEY `sess_id` (`sess_id`);

--
-- Klíče pro tabulku `support_annoucements`
--
ALTER TABLE `support_annoucements`
  ADD PRIMARY KEY (`annoucement_id`);

--
-- Klíče pro tabulku `support_annoucement_assign`
--
ALTER TABLE `support_annoucement_assign`
  ADD PRIMARY KEY (`log_id`),
  ADD UNIQUE KEY `annoucement_user_id` (`annoucement_user_id`,`annoucement_id`);

--
-- Klíče pro tabulku `support_cron_tasks_overview`
--
ALTER TABLE `support_cron_tasks_overview`
  ADD PRIMARY KEY (`task_id`);

--
-- Klíče pro tabulku `support_cron_tasks_queue`
--
ALTER TABLE `support_cron_tasks_queue`
  ADD PRIMARY KEY (`queue_id`);

--
-- Klíče pro tabulku `support_departments`
--
ALTER TABLE `support_departments`
  ADD PRIMARY KEY (`department_id`),
  ADD UNIQUE KEY `departements_code` (`department_code`);

--
-- Klíče pro tabulku `support_groups`
--
ALTER TABLE `support_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `group_code` (`group_code`);

--
-- Klíče pro tabulku `support_internal_remote_token`
--
ALTER TABLE `support_internal_remote_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adress` (`app_code`),
  ADD KEY `function` (`function`),
  ADD KEY `token` (`token`(255));

--
-- Klíče pro tabulku `support_ip_bans`
--
ALTER TABLE `support_ip_bans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`);

--
-- Klíče pro tabulku `support_issues`
--
ALTER TABLE `support_issues`
  ADD PRIMARY KEY (`issue_id`);

--
-- Klíče pro tabulku `support_issues_text`
--
ALTER TABLE `support_issues_text`
  ADD PRIMARY KEY (`text_id`),
  ADD UNIQUE KEY `issue_lang` (`issue_lang`,`issue_id`);

--
-- Klíče pro tabulku `support_resources`
--
ALTER TABLE `support_resources`
  ADD PRIMARY KEY (`res_id`);

--
-- Klíče pro tabulku `support_resources_in_department`
--
ALTER TABLE `support_resources_in_department`
  ADD PRIMARY KEY (`log_id`);

--
-- Klíče pro tabulku `support_resources_in_groups`
--
ALTER TABLE `support_resources_in_groups`
  ADD PRIMARY KEY (`log_id`),
  ADD UNIQUE KEY `res_id` (`res_id`);

--
-- Klíče pro tabulku `support_sessions`
--
ALTER TABLE `support_sessions`
  ADD PRIMARY KEY (`sess_id`),
  ADD KEY `sess_user_internal_id` (`sess_user_internal_id`);

--
-- Klíče pro tabulku `support_settings`
--
ALTER TABLE `support_settings`
  ADD PRIMARY KEY (`sett_id`);

--
-- Klíče pro tabulku `support_supporters_signatures`
--
ALTER TABLE `support_supporters_signatures`
  ADD PRIMARY KEY (`sig_id`),
  ADD UNIQUE KEY `res_id` (`res_id`,`user_id`);

--
-- Klíče pro tabulku `support_tickets`
--
ALTER TABLE `support_tickets`
  ADD PRIMARY KEY (`ticket_id`),
  ADD KEY `ticket_status` (`ticket_status`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `ticket_level` (`ticket_level`),
  ADD KEY `ticket_status_2` (`ticket_status`);

--
-- Klíče pro tabulku `support_tickets_messages`
--
ALTER TABLE `support_tickets_messages`
  ADD PRIMARY KEY (`msg_id`);

--
-- Klíče pro tabulku `support_tickets_messages_who_saw`
--
ALTER TABLE `support_tickets_messages_who_saw`
  ADD PRIMARY KEY (`record_id`),
  ADD UNIQUE KEY `msg_id` (`msg_id`,`user_id`);

--
-- Klíče pro tabulku `support_tokens`
--
ALTER TABLE `support_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD UNIQUE KEY `token_value` (`token_value`) USING BTREE,
  ADD KEY `user_internal_id` (`user_internal_id`);

--
-- Klíče pro tabulku `support_types`
--
ALTER TABLE `support_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Klíče pro tabulku `support_users_creditals`
--
ALTER TABLE `support_users_creditals`
  ADD PRIMARY KEY (`user_internal_id`),
  ADD KEY `password` (`password`);

--
-- Klíče pro tabulku `support_users_data`
--
ALTER TABLE `support_users_data`
  ADD PRIMARY KEY (`user_internal_id`);

--
-- Klíče pro tabulku `support_users_in_departments`
--
ALTER TABLE `support_users_in_departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_internal_id` (`user_internal_id`,`department_id`);

--
-- Klíče pro tabulku `support_users_personal_informations`
--
ALTER TABLE `support_users_personal_informations`
  ADD PRIMARY KEY (`user_internal_id`),
  ADD UNIQUE KEY `login_name` (`login_name`),
  ADD UNIQUE KEY `user_portal_id` (`user_portal_id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Klíče pro tabulku `support_user_rules`
--
ALTER TABLE `support_user_rules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_internal_id` (`user_internal_id`,`right_id`),
  ADD KEY `user_id` (`user_internal_id`),
  ADD KEY `right_id` (`right_id`);

--
-- Klíče pro tabulku `support_user_rules_list`
--
ALTER TABLE `support_user_rules_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sub_right` (`sub_right`,`name`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `support_action_log`
--
ALTER TABLE `support_action_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_annoucements`
--
ALTER TABLE `support_annoucements`
  MODIFY `annoucement_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_annoucement_assign`
--
ALTER TABLE `support_annoucement_assign`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_cron_tasks_overview`
--
ALTER TABLE `support_cron_tasks_overview`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pro tabulku `support_cron_tasks_queue`
--
ALTER TABLE `support_cron_tasks_queue`
  MODIFY `queue_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_departments`
--
ALTER TABLE `support_departments`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_groups`
--
ALTER TABLE `support_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_internal_remote_token`
--
ALTER TABLE `support_internal_remote_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_ip_bans`
--
ALTER TABLE `support_ip_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_issues`
--
ALTER TABLE `support_issues`
  MODIFY `issue_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_issues_text`
--
ALTER TABLE `support_issues_text`
  MODIFY `text_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_resources`
--
ALTER TABLE `support_resources`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_resources_in_department`
--
ALTER TABLE `support_resources_in_department`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_resources_in_groups`
--
ALTER TABLE `support_resources_in_groups`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_sessions`
--
ALTER TABLE `support_sessions`
  MODIFY `sess_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_settings`
--
ALTER TABLE `support_settings`
  MODIFY `sett_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pro tabulku `support_supporters_signatures`
--
ALTER TABLE `support_supporters_signatures`
  MODIFY `sig_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_tickets`
--
ALTER TABLE `support_tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_tickets_messages`
--
ALTER TABLE `support_tickets_messages`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_tickets_messages_who_saw`
--
ALTER TABLE `support_tickets_messages_who_saw`
  MODIFY `record_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_tokens`
--
ALTER TABLE `support_tokens`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_types`
--
ALTER TABLE `support_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_users_in_departments`
--
ALTER TABLE `support_users_in_departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `support_users_personal_informations`
--
ALTER TABLE `support_users_personal_informations`
  MODIFY `user_internal_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `support_users_personal_informations`
auto_increment = 10;

--
-- AUTO_INCREMENT pro tabulku `support_user_rules`
--
ALTER TABLE `support_user_rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



--
-- AUTO_INCREMENT pro tabulku `support_user_rules_list`
--
ALTER TABLE `support_user_rules_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
