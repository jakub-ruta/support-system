# create new cron for delete ip and browser from tickets

INSERT INTO `support_cron_tasks_overview` 
(`task_id`, `task_name`, `task_enabled`, `task_default_priority`, `task_repeat`,
 `task_default_data`, `task_path`, `task_long`, `task_default_due`,
 `task_default_expiry`) 
VALUES (NULL, 'Tickets - remove IP and browser', '1', '5', '24 hours',
 NULL, 'TicketsUtils/autoRemoveIP', '1', '0', '3600');

ALTER TABLE `support_tickets_messages` CHANGE `msg_type` `msg_type` SET('user','admin','comment','system_note','admin_note', 'user_note','ratings') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'user';