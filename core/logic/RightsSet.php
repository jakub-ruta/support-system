<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RightsSet
 *
 * @author jakub
 */
class RightsSet {

    public static function filter($rights, $departments) {
        $db = MysqliDb::getInstance();
        $deps = array();
        foreach ($departments as $dep) {
            if ($dep['user_role'] > 3) {
                return $rights;
            }
            $deps[] = $dep['department_id'];
        }
        $resources = $db->where("l.dep_id", $deps, "IN")
                ->join("resources r", "l.res_id=r.res_id")
                ->get("resources_in_department l");
        $new_rights = array();
        foreach ($rights as $key => $value) {
            foreach ($resources as $res) {
                if (substr($value['name'], 0, strlen("res_" . $res['res_code'])) == "res_" . $res['res_code']) {
                    $new_rights[] = $value;
                    break;
                }
            }
        }
        return $new_rights;
    }

}
