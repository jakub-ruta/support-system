<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuBuilder
 *
 * @author jakub
 */
class MenuBuilder {

    public static function build_menu() {

        $mb = MenuBuilder_new::getInstance();
        $mb->addToMenu("Tickets", "My tickets", "user-tickets", false, "fas fa-envelope");
        if (RightsUtils::gI()->getOneTrueWithSuffix("_admin", "index")) {
            $mb->addToMenu("Tickets", "Tickets", "tickets/open", false, "fas fa-envelope");
            $mb->addToMenu("Settings", "Preset replies", "text-blocks", false, "fas fa-reply");
        }
        if (User::get_instance()->getRuleValue("global_resources", "view")) {
            $mb->addToMenu("Resources admin", "Resources", "resources", false, "fas fa-project-diagram");
            $mb->addToMenu("Resources admin", "Groups", "groups", false, "far fa-object-group");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_ratings", "view")) {
            $mb->addToMenu("Statistic", "Ticket ratings", "ratings", false, "fas fa-star-half-alt");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_response-times", "view")) {
            $mb->addToMenu("Statistic", "Response times", "response-times", false, "fas fa-chart-bar");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_timeline", "view")) {
            $mb->addToMenu("Statistic", "Timeline", "timeline", false, "fas fa-list-alt");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_log", "view")) {
            $mb->addToMenu("Statistic", "Supporter log", "log", false, "far fa-eye");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_master_see", "issues")) {
            $mb->addToMenu("Issues", "Issues", "issues", false, "fas fa-bug");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_master_see", "KB")) {
            $mb->addToMenu("Knowlage Base", "Articles", "kb/articles", false, "fas fa-lightbulb");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_ban", "view")) {
            $mb->addToMenu("Tools", "Ban", "ban", false, "fas fa-ban");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_auto-reply", "view")) {
            $mb->addToMenu("Settings", "Auto reply", "auto-reply", false, "fas fa-magic");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_email_request", "view")) {
            $mb->addToMenu("Tools", "Email request", "tools/email-request", false, "fas fa-mail-bulk");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_email-tool", "view")) {
            $mb->addToMenu("Tools", "Email tool", "tools/email-tool", false, "fas fa-envelope");
        }
        if (User::get_instance()->getRuleValue("global_resources", "view")) {
            $mb->addToMenu("Resources admin", "Resources", "resources", false, "fas fa-project-diagram");
            $mb->addToMenu("Resources admin", "Groups", "groups", false, "far fa-object-group");
        }
        if (User::get_instance()->getRuleValue("global_stats", "view")) {
            $mb->addToMenu("Statistics", "Stats", "stats", false, "fas fa-chart-area");
        }
        if (User::get_instance()->getRuleValue("global_status", "view")) {
            $mb->addToMenu("Tickets", "Status", "status", false, "fas fa-info");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_types", "view")) {
            $mb->addToMenu("Resources admin", "Types", "types", false, "fab fa-stack-exchange");
        }
        if (RightsUtils::gI()->getOneTrueWithSuffix("_master_see", "supporters")) {
            $mb->addToMenu("Tools", "Supporters", "supporters", false, "fas fa-users-cog");
        }
    }

    //put your code here
}
