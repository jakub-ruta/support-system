<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketsUtils
 *
 * @author jakub
 */
class TicketsUtils {

    protected static $_instance;
    protected $db;
    public $levels = array(1 => "Level 1", 2 => "Level 2", 3 => "Level 3",
        4 => "Head", 5 => "Devs");
    public $status = array("open" => "Open", "pending" => "Pending",
        "waiting" => "Waiting", "answered" => "Answered",
        "closed" => "Closed", "archived" => "Archived");
    public $categories = array("open" => "Open", "all_open" => "All", "pending" => "Pending",
        "waiting" => "Waiting", "answered" => "Answered",
        "closed" => "Closed", "archived" => "Archived", "custom" => "Custom");
    public $visibleData = array("ticket_id", "res_code", "ticket_origin",
        "user_internal_id", "login_name", "nickname", "mail", "timestamp_registered");

    /**
     *
     * @return TicketsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new TicketsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return TicketsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getOneTicket($tck_id) {
        $ticket = $this->getOne($tck_id);
        $this->canAdminViewTicket($ticket);
        return $ticket;
    }

    protected function getOne($tck_id) {
        return $this->db
                        ->where("l.ticket_id", $tck_id)
                        ->join("resources r", "l.resource_id=r.res_id", "LEFT")
                        ->join("types r2", "l.type_id=r2.type_id", "LEFT")
                        ->join("users_personal_informations r3", "l.user_id=r3.user_internal_id")
                        ->getOne("tickets l", "l.*, r.*, r2.type_name, r2.type_code, r2.type_id, r3.*");
    }

    public function canAdminViewTicket($ticket) {
        $rights = User::getInstance();
        if (!$rights->getRuleValue("res_" . $ticket['res_code'] . "_admin", "index")) {
            throw new RightException("You do not have right to view this ticket that belengos to this resource");
        }
        if (!$rights->getRuleValue("res_" . $ticket['res_code'] . "_" . $ticket['type_code'],
                        "level_" . $ticket['ticket_level'])) {
            throw new RightException("You are not authorozed to view this ticket.");
        }
        if ($ticket['ticket_status'] == "archived" && (!$rights->getRuleValue("res_" . $ticket['res_code'] . "_" . $ticket['type_code'],
                        "level_3") && !$rights->getRuleValue("res_" . $ticket['res_code'] . "_" . $ticket['type_code'],
                        "level_4") && !$rights->getRuleValue("res_" . $ticket['res_code'] . "_" . $ticket['type_code'],
                        "level_5"))) {
            throw new RightException("You are not authorozed to view this archived ticket.");
        }
        return true;
    }

    public function getTicketMessages($id) {
        return $this->db->where("l.ticket_id", $id)->
                        orderBy("l.msg_created")->
                        join("users_personal_informations r", "l.msg_admin_id=r.user_internal_id", "LEFT")
                        ->get("tickets_messages l", null, "l.*, r.avatar, r.login_name");
    }

    public function getWhoSeen($msg_id) {
        return $this->db->where("l.msg_id", $msg_id)
                        ->join("users_personal_informations r", "l.user_id=r.user_internal_id")
                        ->get("tickets_messages_who_saw l", null, "l.*, r.login_name, r.nickname, r.avatar");
    }

    public function getTickets($admin_id, $res_id, $status = "open", $offset = 0, $count = 200, $filter = null) {
        $resource = ResourcesUtils::gI()->getOne($res_id);
        $types = TypesUtils::gI()->getByResource($res_id);
        $copied_db = $this->db->copy();
        $copied_db->join("users_personal_informations r", "l.admin_id=r.user_internal_id", "LEFT")
                ->join("users_personal_informations r2", "l.user_id=r2.user_internal_id", "LEFT")
                ->join("types t", "l.type_id=t.type_id", "LEFT");
        $this->buildWhereQuery($copied_db, $resource, $types);
        if ($filter) {
            $this->buildFilter($copied_db, $filter);
        } else {
            $this->buildStatusAdminQuery($copied_db, $admin_id, $status);
        }
        $copied_db->orderBy("l.ticket_open_post_time", "ASC");
        $data = $copied_db->get("tickets l", array($offset, $count),
                "l.*,"
                . " r.user_internal_id AS admin_id,"
                . " r.login_name AS admin_name,"
                . " r.nickname AS admin_nickname,"
                . " r.avatar AS admin_avatar,"
                . " r2.*,"
                . " t.*");
        //throw new Exception($copied_db->getLastQuery());
        return $data;
    }

    protected function buildWhereQuery($copied_db, $resource, $types) {
        $have_some_right = false;
        foreach ($types as $type) {
            foreach ($this->levels as $level => $lvl) {
                if (User::getInstance()->getRuleValue("res_" . $resource['res_code'] . "_" . $type['type_code'],
                                "level_" . $level)) {
                    $have_some_right = true;
                    $copied_db->orwhere("(l.type_id=? AND l.ticket_level=? AND l.resource_id=?)", array($type['type_id'], $level, $resource['res_id']));
                }
            }
        }
        if (!$have_some_right)
            throw new RightException("You do not have right to any type!");
    }

    public function buildFilter($copied_db, $filter) {
        $max_age = $filter['max_age'];
        unset($filter['max_age']);
        $copied_db->having("ticket_last_post_time", date("Y-m-d H:i:s", time() - ($max_age * 24 * 60 * 60)), ">");
        foreach ($filter as $key => $value) {
            if ($value == -1 || $value == null) {
                continue;
            }
            $copied_db->having("l." . $key, $value);
        }
    }

    protected function buildStatusAdminQuery($copied_db, $admin_id, $status) {
        if ($status == "" || $status == null) {
            $status = "open";
        }
        switch ($status) {
            case 'open':
                $copied_db->having("l.ticket_status", "open");
                $copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            case 'pending':
                $copied_db->having("l.ticket_status", "pending");
                $copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            case 'waiting':
                $copied_db->having("l.ticket_status", "waiting");
//$copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            case 'answered':
                $copied_db->having("l.ticket_status", "answered");
                $copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            case 'closed':
                $copied_db->having("l.ticket_status", "closed");
//$copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            case 'archived':
                $copied_db->having("l.ticket_status", "archived");
                $copied_db->having("(l.admin_id=? OR l.admin_id IS NULL)", array($admin_id));
                break;
            default:
                $copied_db->having("l.ticket_status", "open");
                break;
        }
    }

    public function getCounts($admin_id, $res_id) {
        $types = TypesUtils::gI()->getByResource($res_id);
        $resource = ResourcesUtils::gI()->getOne($res_id);
        $copied_db = $this->db->copy();
        $this->buildWhereQuery($copied_db, $resource, $types);
        $data = $copied_db->getOne("tickets l",
                " sum(case when l.ticket_status = 'open' then 1 else 0 end) AS all_open,"
                . " sum(case when l.ticket_status = 'answered' && (l.admin_id IS NULL || l.admin_id = $admin_id) then 1 else 0 end) AS answered,"
                . " sum(case when l.ticket_status = 'open' && (l.admin_id = $admin_id) then 1 else 0 end) AS my_open, "
                . " sum(case when l.ticket_status = 'waiting' then 1 else 0 end) AS waiting, "
                . " sum(case when l.ticket_status = 'closed' then 1 else 0 end) AS closed, "
                . " sum(case when l.ticket_status = 'archived' && (l.admin_id IS NULL || l.admin_id = $admin_id) then 1 else 0 end) AS archived, "
                . " sum(case when (l.ticket_status = 'open' && (l.admin_id IS NULL || l.admin_id = $admin_id)) then 1 else 0 end) AS open, "
                . " sum(case when l.ticket_status = 'pending' && (l.admin_id IS NULL || l.admin_id = $admin_id) then 1 else 0 end) AS pending"
        );
        return $data;
    }

    public function getAllUserTickets($user_id) {
        return $this->db
                        ->where("l.user_id", $user_id)
                        ->orderBy("l.ticket_last_post_time")
                        ->join("resources r", "l.resource_id = r.res_id", "LEFT")
                        ->join("types r2", "l.type_id = r2.type_id", "LEFT")
                        ->join("users_personal_informations r3", "l.admin_id = r3.user_internal_id", "LEFT")
                        ->get("tickets l", null, "l.*, r.*, r2.*, r3.user_internal_id AS admin_id, "
                                . "r3.login_name AS admin_name, r3.nickname AS admin_nickname, "
                                . "r3.avatar AS admin_avatar");
    }

    public function addMessage($type, $msg, $ticket_id, $admin_id = null) {
        if (!in_array($type, array("admin", "user", "comment", "admin_note", "user_note", "system_note", "ratings"))) {
            throw new Exception("Wrong msg type");
        }
        $this->db->insert("tickets_messages", array("ticket_id" => $ticket_id,
            "msg_ip" => SpravceIP::getUserIpAddr(), "msg_browser" => SpravceIP::getUserBrowser(),
            "msg_text" => $msg, "msg_type" => $type, "msg_admin_id" => $admin_id));
        if ($type != "comment") {
            $this->db->where("ticket_id", $ticket_id)->
                    update("tickets", array("ticket_last_post_time" => date("Y-m-d H:i:s")));
        }
    }

    public function alterStatus($status, $tck_id) {
        if (array_key_exists($status, $this->status)) {
            $this->db->where("ticket_id", $tck_id)
                    ->update("tickets", array("ticket_status" => $status));
        } else {
            throw new Exception("wrong status");
        }
    }

    public function alterAdmin($admin_id, $tck_id) {
        if ($admin_id < 10) {
            $admin_id = null;
        }
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("admin_id" => $admin_id));
    }

    public function alterResource($res_id, $tck_id) {
        $type = TypesUtils::gI()->getByResource($res_id);
        bdump($type);
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("resource_id" => $res_id, "type_id" => $type[0]['type_id']));
        //throw new Exception($this->db->getLastQuery());
    }

    public function alterLevel($level, $tck_id) {
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("ticket_level" => $level));
    }

    public function alterType($type_id, $tck_id) {
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("type_id" => $type_id));
    }

    public function getUsedTypes() {
        return $this->db->where("l.resource_id", ResourcesUtils::gI()->getActive())
                        ->join("types r", "l.type_id = r.type_id")
                        ->groupBy("l.type_id")
                        ->get("tickets l");
    }

    public function getUsedSupporters() {
        return $this->db->where("l.resource_id", ResourcesUtils::gI()->getActive())
                        ->join("users_personal_informations r", "l.admin_id = r.user_internal_id")
                        ->groupBy("l.admin_id")
                        ->get("tickets l");
    }

    public function autoCloseTickets() {
        $for_close = $this->db
                ->where("ticket_status", "answered")
                ->where("ticket_last_post_time", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 14), "<")
                ->get("tickets");
        foreach ($for_close as $tck) {
            $this->alterStatus("closed", $tck['ticket_id']);
            $this->util->addMessage("system_note", "Ticket status changed to Closed", $tck['ticket_id'], 0);
        }
        $for_archive = $this->db
                ->where("ticket_status", "closed")
                ->where("ticket_last_post_time", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 60), "<")
                ->get("tickets");
        foreach ($for_archive as $tck) {
            $this->alterStatus("archived", $tck['ticket_id']);
            $this->util->addMessage("system_note", "Ticket status changed to Archived", $tck['ticket_id'], 0);
        }
    }

    public function autoUnsnooze() {
        $for_unsooze = $this->db->where("ticket_snooze_until", date("Y-m-d H:i:s"), "<")
                ->get("tickets");
        foreach ($for_unsooze as $tck) {
            $this->alterStatus("open", $tck['ticket_id']);
            $this->util->addMessage("system_note", "Ticket status changed to Open from snooze", $tck['ticket_id'], 0);
            $this->alterSnooze(null, $tck['ticket_id']);
        }
    }

    public function autoUnasigne() {
        $for_close = $this->db
                ->where("ticket_status", array("open", "pending"), "IN")
                ->where("ticket_open_post_time", date("Y-m-d H:i:s", time() - 60 * 60 * 50), "<")
                ->get("tickets");
        foreach ($for_close as $tck) {
            $this->alterAdmin(null, $tck['ticket_id']);
            $this->util->addMessage("system_note", "Ticket assigned to none", $tck['ticket_id'], 0);
        }
    }

    public function alterCanClose($state, $tck_id) {
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("ticket_can_be_closed_by_user" => $state));
    }

    public function alterSnooze($time, $tck_id) {
        if ($time != null) {
            $time = date("Y-m-d H:i:s", $time);
        }
        $this->db->where("ticket_id", $tck_id)
                ->update("tickets", array("ticket_snooze_until" => $time));
    }

    public function autoRemoveIP() {
        $this->db
                ->where("r.ticket_last_post_time", date("Y-m-d H:i:s", time() - 60 * 60 * 24 * 30), "<")
                ->where("(r.ticket_status=? OR r.ticket_status=?)", array("closed", "archived"))
                ->join("tickets r", "l.ticket_id=r.ticket_id")
                ->update("tickets_messages l", array("l.msg_ip" => null, "l.msg_browser" => null));
    }

}
