<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SupportersUtils
 *
 * @author jakub
 */
class SupportersUtils {

    /**
     *
     * @var class Mysqli
     */
    protected $db;
    static $_instance;

    /**
     *
     * @return SupportersUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new SupportersUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return SupportersUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getEditable() {
        $admin_dep = DepartmentsUtils::gI()->getUserDepartments(User::getUserId(), true);
        foreach ($admin_dep as $value) {
            $this->db->orwhere("l.department_id", $value['department_id']);
        }

        $this->db->groupBy("l.user_internal_id")
                ->join("users_personal_informations r", "l.user_internal_id=r.user_internal_id");
        return $this->db->get("users_in_departments l", null, "l.*, r.*, COUNT(DISTINCT l.department_id) AS DEP_count");
    }

    public function getAllForUser($user_id) {

        return $this->db->where("user_id", $user_id)->get("supporters_signatures");
    }

    public function edit($res_id, $user_id, $text) {
        $updateColumns = array("text");
        $lastInsertId = "sig_id";
        $this->db->onDuplicate($updateColumns, $lastInsertId)
                ->insert('supporters_signatures',
                        array("user_id" => $user_id,
                            "res_id" => $res_id,
                            "text" => $text));
    }

    public function getSignature($res_id, $user_id = null) {
        if ($user_id == null) {
            $user_id = User::getUserId();
        }
        return '<br><br>' . $this->db->where("res_id", $res_id)->
                        where("user_id", $user_id)->getValue("supporters_signatures", "text");
    }

}
