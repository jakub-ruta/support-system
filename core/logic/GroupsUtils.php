<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupsUtils
 *
 * @author jakub
 */
class GroupsUtils extends Utils_new {

    protected static $_instance;
    protected $table_name = "groups";
    protected $table_key = "group_id";
    protected $displayAble = array("group_id", "group_code", "group_name");
    protected $editAble = array("group_name");
    protected $new_fields = array("group_name", "group_code");

    /**
     *
     * @return GroupsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new GroupsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return GroupsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getByResource($res_id) {
        return $this->db->join("groups r", "l.group_id=r.group_id")
                        ->where("res_id", $res_id)
                        ->get("resources_in_groups l");
    }

    public function change($res_id, $group_id) {
        $this->db->where("res_id", $res_id)->delete("resources_in_groups", 1);
        $this->db->insert("resources_in_groups", array("res_id" => $res_id, "group_id" => $group_id));
    }

}
