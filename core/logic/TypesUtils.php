<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypesUtils
 *
 * @author jakub
 */
class TypesUtils extends Utils_new {

    protected static $_instance;
    protected $table_name = "types";
    protected $table_key = "type_id";
    protected $displayAble = array("type_id", "resource_id", "type_name", "type_vissible", "type_code");
    protected $editAble = array("type_name", "type_vissible");
    protected $new_fields = array("type_name", "type_code", "resource_id");
    protected $rights_admin = array("index", "holiday", "can_delegate");
    protected $rights_master = array("admin_ip", "IP", "ratings", "who_saw", "supporters", "issues");

    /**
     *
     * @return TypesUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new TypesUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return TypesUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getByResource($id) {
        return $this->db->where("resource_id", $id)->get($this->table_name);
    }

    public function repairRights() {
        $types = $this->db->join("resources r", "l.resource_id=r.res_id")
                ->get("types l");
        bdump($types);
        foreach ($types as $key => $value) {
            foreach ($this->rights_admin as $right) {
                RightsUtils::gI()->createRight("res_" . $value['res_code'] . "_admin", $right, 1);
            }
            foreach ($this->rights_master as $right) {
                RightsUtils::gI()->createRight("res_" . $value['res_code'] . "_master_see", $right, 1);
            }

            foreach (TicketsUtils::gI()->levels as $lvl => $level) {
                if ($lvl < 4) {
                    $granteable = 1;
                } else {
                    $granteable = 0;
                }
                RightsUtils::gI()->createRight("res_" . $value['res_code'] . "_" . $value['type_code'],
                        "level_" . $lvl, $granteable);
            }
        }
        User::getInstance()->rebaseRules();
    }

    public function getEnabled($res_code) {
        return $this->db->where("l.type_vissible", 1)
                        ->where("r.res_code", $res_code)
                        ->join("resources r", "l.resource_id=r.res_id")
                        ->get("types l");
    }

    public function getOneByCodeAndResource($type, $res_id) {
        return $this->db->where("type_code", $type)
                        ->where("resource_id", $res_id)
                        ->getOne("types");
    }

}
