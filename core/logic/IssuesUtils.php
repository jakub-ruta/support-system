<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IssuesUtils
 *
 * @author jakub
 */
class IssuesUtils extends Utils_new {

    protected $table_name = "issues";
    protected $table_key = "issue_id";
    protected $displayAble = null;
    protected $editAble = array("issue_enabled_from", "issue_enable_to", "issue_name");
    protected $new_fields = null;

    /**
     *
     * @var class Mysqli
     */
    protected $db;
    static $_instance;

    /**
     *
     * @return IssuesUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new IssuesUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return IssuesUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getVissible() {
        $res = $this->getResources();
        if (count($res) > 0) {
            foreach ($res as $value) {
                $this->db->orWhere("l.res_id", $value['res_id']);
            }
            $this->db->join("users_personal_informations r", "l.issue_created_by=r.user_internal_id");
            $this->db->join("resources res", "l.res_id=res.res_id");
            return $this->db->get("issues l", null, "l.*, r.user_internal_id, r.login_name, r.nickname, r.avatar, res.res_name");
        }
        return array();
    }

    public function getResources() {
        $admin_res = ResourcesUtils::gI()->getAll();
        $new = array();
        foreach ($admin_res as $key => $res) {
            if (User::getInstance()->getRuleValue("res_" . $res['res_code'] . "_master_see", "issues")) {
                $new[$res['res_code']] = $res;
            }
        }
        return $new;
    }

    public function getTexts($id) {
        return $this->db->where("issue_id", $id)->get("issues_text");
    }

    public function getIssuesForResource($res, $lang = "en") {
        $issues = $this->db
                ->where("l.res_id", $res)
                ->where("l.issue_enabled_from", date("Y-m-d H:i:s"), "<")
                ->where("l.issue_enable_to", date("Y-m-d H:i:s"), ">")
                ->get("issues l");
        foreach ($issues as $key => $value) {
            $text = $this->db->where("issue_id", $value['issue_id'])
                    ->get("issues_text");
            $texts = ArrayUtils::makeKeyValueArray($text, "issue_lang", "issue_text");
            $issues[$key]['text'] = (isset($texts[$lang]) ? $texts[$lang] : $texts['en']);
        }
        return $issues;
    }

    public function addType($issue_id, $new_type) {
        if (!isset($new_type) || $new_type == '')
            return;
        $issue = $this->getOne($issue_id);
        $types = json_decode($issue['issues_types'], true);
        foreach ($types as $value) {
            if ($value == $new_type) {
                return;
            }
        }
        $types[] = $new_type;
        $this->db->where("issue_id", $issue_id)
                ->update("issues", array("issues_types" => json_encode($types)));
    }

    public function removeType($issue_id, $remove_type) {
        $issue = $this->getOne($issue_id);
        $types = json_decode($issue['issues_types'], true);
        foreach ($types as $key => $value) {
            if ($value == $remove_type) {
                unset($types[$key]);
            }
        }
        $types = array_values($types);
        $this->db->where("issue_id", $issue_id)
                ->update("issues", array("issues_types" => json_encode($types)));
    }

    public function editText($URL_params, $data) {
        CSRFUtils::gI()->checkCSRF($data['csrf']);
        $this->db->where("text_id", $data['text_id'])
                ->update("issues_text", array("issue_lang" => $data['lang'],
                    "issue_text" => $data['text']));
    }

    public function addText($issue_id, $data) {
        $this->db->insert("issues_text", array("issue_lang" => $data['lang'],
            "issue_id" => $issue_id));
    }

    public function createNew($res_id) {
        $this->db->insert("issues", array("res_id" => $res_id,
            "issue_created_by" => User::getUserId(),
            "issue_enable_to" => date("Y-m-d H:i:s", time() + 60 * 60 * 24 * 90)));
        $id = $this->db->getInsertId();
        $this->db->insert("issues_text", array("issue_id" => $id,
            "issue_lang" => "en",
        ));
    }

}
