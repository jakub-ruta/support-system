<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResourcesUtils
 *
 * @author jakub
 */
class ResourcesUtils extends Utils_new {

    protected static $_instance;
    public $displayAble = array("res_id", "res_code", "res_name", "res_public", "group_name");
    public $editAble = array("res_name", "res_public");
    public $new_fields = array("res_name", "res_code");
    protected $table_name = "resources";
    protected $table_key = "res_id";

    /**
     *
     * @return ResourcesUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new ResourcesUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return ResourcesUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getAll() {
        return $this->db->join("resources_in_groups r", "l.res_id=r.res_id", "LEFT")
                        ->join("groups r2", "r.group_id=r2.group_id", "LEFT")
                        ->get("resources l", null, "l.*, r2.group_name, r2.group_code");
    }

    public function getAdminsForResource($res_id) {
        $res = $this->getOne($res_id);
        $admins = User::getInstance()->getAllUsers();
        foreach ($admins as $key => $value) {
            $admins[$key]['holiday'] = User::getInstance()
                    ->getRightForUser($value['user_internal_id'], "res_" . $res['res_code'] . "_admin", "holiday");
            $admins[$key]['can_delegate'] = User::getInstance()
                    ->getRightForUser($value['user_internal_id'], "res_" . $res['res_code'] . "_admin", "can_delegate");
            $admins[$key]['index'] = User::getInstance()
                    ->getRightForUser($value['user_internal_id'], "res_" . $res['res_code'] . "_admin", "index");
        }
        return $admins;
    }

    public function getActive() {
        if (isset($_SESSION['temp']['active_resource'])) {
            return $_SESSION['temp']['active_resource'];
        } else {
            $_SESSION['temp']['active_resource'] = $this->getMyResources()[0]['res_id'];
        }
    }

    public function getMyResources($part = "_admin", $sub_right = "index") {
        if (!in_array($part, array("_admin", "_master_see"))) {
            throw new Exception("Wrong right type");
        }
        $res = $this->getAll();
        bdump($res);
        $user = User::getInstance();
        foreach ($res as $key => $value) {
            if (!$user->getRuleValue("res_" . $value['res_code'] . $part, $sub_right)) {
                unset($res[$key]);
            }
        }
        if (!count($res) > 0) {
            throw new RightException("You do not have right to view any resource");
        }
        return $res;
    }

    public function setResource($new) {
        $res = $this->getMyResources();
        $res = ArrayUtils::makeKeyArray($res, "res_id");
        bdump($res);
        if (key_exists($new, $res)) {
            $_SESSION['temp']['active_resource'] = $new;
        } else {
            throw new RightException("You do not have right to view requested resource");
        }
    }

    public function getAssigned($res_id) {
        return $this->db
                        ->join("departments r", "l.dep_id = r.department_id")
                        ->where("l.res_id", $res_id)
                        ->get("resources_in_department l");
    }

    public function addDepartment($res_id, $dep_id) {
        $this->db->insert("resources_in_department", array("res_id" => $res_id, "dep_id" => $dep_id));
    }

    public function removeDepartment($res_id, $dep_id) {
        $this->db
                ->where("dep_id", $dep_id)
                ->where("res_id", $res_id)
                ->delete("resources_in_department", 1);
    }

    public function getEnabled() {
        return $this->db->where("res_public",)->get("resources");
    }

    public function getOneByCode($code) {
        return $this->db->where("res_code", $code)->getOne("resources");
    }

}
