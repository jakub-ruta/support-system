<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserTicketsUtils
 *
 * @author jakub
 */
class UserTicketsUtils {

    /**
     *
     * @var class Mysqli
     */
    protected $db;
    protected static $_instance;

    /**
     *
     * @return UserTicketsUtils Returns the current instance.
     */
    static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new UserTicketsUtils();
        }
        return self::$_instance;
    }

    /**
     *
     * @return UserTicketsUtils Returns the current instance.
     */
    static function gI() {
        return self::getInstance();
    }

    function __construct() {
        self::$_instance = $this;
        $this->db = MysqliDb::getInstance();
    }

    public function getMyTickets($user_id) {
        return TicketsUtils::gI()->getAllUserTickets($user_id);
    }

    public function getMessages($tck_id) {
        return TicketsUtils::gI()->getTicketMessages($tck_id);
    }

    public function getOne($tck_id, $user_id) {
        $tck = TicketsUtils::gI()->getOneTicket($tck_id);
        if ($tck['user_id'] != $user_id) {
            throw new RightException("This is not your ticket!");
        }
        return $tck;
    }

    public function addReply($tck_id, $msg) {
        TicketsUtils::gI()->addMessage("user", $msg, $tck_id);
        TicketsUtils::gI()->alterStatus("open", $tck_id);
        TicketsUtils::gI()->addMessage("user_note", "Status changed to Open", $tck_id);
    }

    public function closeTicket($tck_id) {
        TicketsUtils::gI()->alterStatus("closed", $tck_id);
        TicketsUtils::gI()->addMessage("user_note", "User closed ticket", $tck_id);
    }

    public function createTicket($data) {
        $res = ResourcesUtils::gI()->getOneByCode($data['resource']);
        if (!isset($res['res_id'])) {
            throw new NewTicketException("Wrong ticket resource");
        }
        $type = TypesUtils::gI()->getOneByCodeAndResource($data['type'], $res['res_id']);
        if (!isset($type['type_id'])) {
            throw new NewTicketException("Wrong ticket type");
        }
        $this->db->insert("tickets",
                array("resource_id" => $res['res_id'],
                    "type_id" => $type['type_id'],
                    "user_id" => User::getUserId(),
                    "ticket_subject" => $data['subject'],
        ));
        $id = $this->db->getInsertId();
        if (!is_numeric($id))
            throw new Exception("Somethink went wrong");
        TicketsUtils::gI()->addMessage("user", $data['message'], $id);
        return $id;
    }

    public function setOpenTime($id) {
        $this->db->where("ticket_id", $id)
                ->update("tickets", array("ticket_open_post_time" => date("Y-m-d H:i:s")));
    }

}
