<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usertickets
 *
 * @author jakub
 */
class UserTicketsControler extends Controler {

    /**
     *
     * @var class UserTicketsUtils extends Utils_new
     */
    protected $utils;
    protected $right = null;
    protected $path = "usertickets";
    protected $oneView = "user_ticket";

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 0, false);
        $this->utils = UserTicketsUtils::gI();
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if ($URL_params[0] == "new") {
            $this->newTicket($URL_params);
        } elseif (is_numeric($URL_params[0])) {
            $this->one($URL_params);
        } else {
            $this->my($URL_params);
        }


        bdump($this);
    }

    public function my($URL_params) {
        $this->view = "user_tickets";
        $this->data['tickets'] = $this->utils->getMyTickets(User::getUserId());
    }

    public function one($URL_params) {
        $this->view = "user_ticket";
        try {
            $this->data['ticket'] = $this->utils->getOne($URL_params[0], User::getUserId());
        } catch (RightException $exc) {
            $this->redirectToError($exc->getMessage(), 401);
        }
        if ($URL_params[1] == "add_reply") {
            $this->add_reply($URL_params);
        } else if ($URL_params[1] == "close") {
            $this->closeTicket($URL_params);
        }
        $this->data['replyes'] = $this->utils->getMessages($URL_params[0]);
        $this->data['reply_form'] = $this->createReplyForm($URL_params)->renderAll();
    }

    public function createReplyForm($URL_params) {
        $form = new FormFactory("ticket_reply");
        $form->setAction("user-tickets/" . $URL_params[0] . "/add_reply")
                ->createTextArea("message", null)->placeholder(Lang::str("Write reply here!"))
                ->required();
        $form->createButton("submit", Lang::str("Send"))
                ->Class("btn btn-success float-right")
                ->value(Lang::str("Add reply"));

        return $form;
    }

    public function add_reply($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $this->utils->addReply($URL_params[0], $_POST['message']);
        if ($this->data['ticket']['ticket_status'] != "open")
            $this->utils->setOpenTime($URL_params[0]);
        $this->redirect("user-tickets/" . $URL_params[0]);
    }

    public function closeTicket($URL_params) {
        $this->utils->closeTicket($URL_params[0]);
        $this->addMessage("Ticket has been closed", "success");
        $this->redirect("user-tickets");
    }

    public function newTicket($URL_params) {
        $this->view = "user_ticket_new";
        $res = ResourcesUtils::gI()->getOneByCode((isset($_POST['resource']) ? $_POST['resource'] : $URL_params[1]));
        $only_resource = true;
        if (isset($res['res_id']) && (isset($URL_params[1]) || isset($_POST['resource']))) {
            $only_resource = false;
        }
        $form = $this->createNewTicketForm($URL_params, $only_resource);
        if ($form->isSend()) {
            $id = $this->utils->createTicket($form->getValues());
            $this->redirect("user-tickets/" . $id);
        }

        $this->data['issues'] = IssuesUtils::gI()->getIssuesForResource($res['res_id'], Lang::getLang());
        $this->data['form'] = $form->renderAll();
    }

    public function createNewTicketForm($URL_params, $only_resource) {
        $form = new FormFactory("new_ticket");
        $form->setAction("user-tickets/new");
        $res = ResourcesUtils::gI()->getAll();
        $res[] = array("res_code" => -1, "res_name" => Lang::str("Please select resource"));
        $form->createSelect("resource", Lang::str("Select resource:"))->
                setOptions($res, "res_code", "res_name")->
                setVissible(ResourcesUtils::gI()->getEnabled(), "res_code", "res_name")->
                setPrepend('<i class="fas fa-globe-europe"></i>')
                ->setSelected((isset($URL_params[1]) && !$only_resource ? $URL_params[1] : "-1"))
                ->setDisabled(array(-1))
                ->required()
                ->setOnChange("changeRes(this)");
        if (!$only_resource) {
            $selected_resource = (isset($_POST['resource']) ? $_POST['resource'] : $URL_params[1]);
            $form->createSelect("type", Lang::str("Select ticket type:"))->
                    setOptions(TypesUtils::gI()->getAll(), "type_code", "type_name")->
                    setVissible(TypesUtils::gI()->getEnabled($selected_resource), "type_code", "type_name")->
                    setPrepend('<i class="fas fa-question-circle"></i>')
                    ->required()
                    ->setSelected($URL_params[2])
                    ->setOnChange("changeType(this)");
            $form->createTextInput("subject", Lang::str("Subject:"))
                    ->required()
                    ->placeholder("A brief summary of the ticket");
            $form->createTextArea("message", Lang::str("Your message:"))
                    ->placeholder(Lang::str("Write message here!"))
                    ->Class("form-control mb-20")
                    ->required();
            $form->createButton("submit", Lang::str("Send"))
                    ->Class("btn btn-success float-right")
                    ->value(Lang::str("Create"));
        }
        return $form;
    }

}
