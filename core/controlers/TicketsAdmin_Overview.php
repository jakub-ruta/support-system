<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketsAdmin_Overview
 *
 * @author jakub
 */
class TicketsAdmin_Overview extends Controler {

    /**
     *
     * @var class TicketsUtils
     */
    protected $util;

    /**
     * array for filter ticket based on custom filter
     * @var array|null
     */
    protected $filter = null;

    //put your code here
    public function execute($URL_params) {

        $this->util = TicketsUtils::gI();
        $this->overview($URL_params);
        if ($URL_params[0] == "custom") {
            $form = $this->createSearchForm();
            $this->filter = $form->getValues();
            $this->data['search'] = $form->renderAll();
        }
        $this->prepareTickets($URL_params);
        bdump($this->data);
    }

    public function overview($URL_params) {
        $res = ResourcesUtils::gI();
        $this->view = "admin_tickets";
        $this->data['resources'] = $res->getMyResources();
        $this->data['selected_resource'] = $res->getActive();
        $this->data['ticket_statuses'] = $this->util->categories;
        try {
            $this->data['counts'] = $this->util->getCounts(User::getUserId(), ResourcesUtils::gI()->getActive(), $URL_params[0]);
        } catch (RightException $exc) {
            $this->addMessage("You do not have any right in selected resource", "danger");
            $this->redirect("dashBoard");
        }


        $this->data['search'] = $this->createSearchForm()->renderAll();
    }

    protected function prepareTickets($URL_params) {
        if (isset($_GET['offset']) && is_numeric($_GET['offset']) && $_GET['offset'] > 0) {
            $offset = $_GET['offset'];
        } else {
            $offset = 0;
        }
        $count = 200;
        $this->data['tickets'] = $this->util->getTickets(User::getUserId(),
                ResourcesUtils::gI()->getActive(), $URL_params[0], $offset, $count, $this->filter);
    }

    protected function createSearchForm() {
        $types = $this->util->getUsedTypes();
        $types[] = array("type_id" => -1, "type_name" => "-All-");
        $supporters = $this->util->getUsedSupporters();
        $supporters[] = array("user_internal_id" => -1, "login_name" => "-All-");
        $levels = $this->util->levels;
        $levels["-1"] = "-All-";
        $status = $this->util->status;
        $status["-1"] = "-All-";
        $form = new FormFactory("ticket_search");
        $form->setForm_class("form-inline")->setAction("tickets/custom/search");
        $form->createSelect("type_id", "Type:")->
                setOptions($types, "type_id", "type_name")->
                setSelected(-1)->
                setPrepend('<i class="fas fa-question-circle"></i>');
        $form->createSelect("admin_id", "Supporter:")->
                setOptions($supporters, "user_internal_id", "login_name")->
                setSelected(-1)->
                setPrepend('<i class="fas fa-user-tie"></i>')->
                setSelected(User::getUserId());
        $form->createSelect("ticket_level", "Level:")->
                setOptions($levels)->
                setSelected(-1)->
                setPrepend('<i class="fas fa-level-up-alt"></i>');
        $form->createSelect("ticket_status", "Status:")->
                setOptions($status)->
                setSelected(-1)->
                setPrepend('<i class="fas fa-check-square"></i>');
        $form->createNumberInput("max_age", "Max age:")
                ->setPrepend('<i class="fas fa-hourglass-half"></i>')
                ->setAppend("days")
                ->setMin("1")
                ->value(20);
        $form->createButton("search", "Search")->Class("btn btn-success");
        return $form;
    }

}
