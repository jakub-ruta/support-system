<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GroupsControler
 *
 * @author jakub
 */
class GroupsControler extends TableControlerClass {

    /**
     *
     * @var class TypesUtils extends Utils_new
     */
    protected $utils;
    protected $right = "global_resources";
    protected $path = "groups";
    protected $oneView = "group";

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("global_resources", "view", $URL_params, true, 2, false);
        $this->registerTemplate("template_new", "groups");
        $this->utils = GroupsUtils::gI();
        $this->useRouter($URL_params);
    }

}
