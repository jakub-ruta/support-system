<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketsAdmin_One
 *
 * @author jakub
 */
class TicketsAdmin_One extends Controler {

    /**
     *
     * @var class TicketsUtils
     */
    protected $util;

    //put your code here
    public function execute($URL_params) {

        $this->util = TicketsUtils::gI();
        try {
            $this->oneTicket($URL_params);
        } catch (RightException $exc) {
            $this->redirectToError($exc->getMessage(), 401);
        }
    }

    public function oneTicket($URL_params) {
        $this->data['ticket'] = $ticket = $this->util->getOneTicket($URL_params[0]);

        if ($URL_params[1] == "add_reply") {
            $this->addReply($URL_params, $ticket);
        } elseif ($URL_params[1] == "add_comment") {
            $this->addComment($URL_params, $ticket);
        } elseif ($URL_params[1] == "add_comment") {
            $this->addComment($URL_params, $ticket);
        } elseif ($URL_params[1] == "prevent_close") {
            $this->prevent_close($URL_params, $ticket);
        } elseif ($URL_params[1] == "change") {
            $this->Change($URL_params, $ticket);
        } elseif ($URL_params[1] == "allow_close") {
            $this->allow_close($URL_params, $ticket);
        } elseif ($URL_params[1] == "snooze") {
            $this->snooze($URL_params, $ticket);
        } elseif ($URL_params[1] == "unsnooze") {
            $this->unsnooze($URL_params, $ticket);
        } elseif ($URL_params[1] == "self_asign") {
            $this->selfAssign($URL_params, $ticket);
        } elseif ($URL_params[1] == "delegate") {
            $this->delegate($URL_params, $ticket);
        } else {
            $this->oneTicketDisplay($URL_params, $ticket);
        }
    }

    protected function oneTicketDisplay($URL_params, $ticket) {
        if ($ticket['res_id'] != ResourcesUtils::gI()->getActive()) {
            throw new WrongResourceException($ticket['res_id']);
        }
        $this->data['right'] = $this->resource_rights($ticket);
        $this->data['ticket_vissible'] = $this->util->visibleData;
        $resources = ResourcesUtils::gI()->getAll();
        $types = TypesUtils::gI()->getByResource($this->data['ticket']['resource_id']);
        $supporters = ResourcesUtils::gI()->getAdminsForResource($this->data['ticket']['resource_id']);
        $this->view = "admin_ticket";
        $form = $this->createPropertiesForm($ticket, $resources, $types, $supporters, $URL_params);
        $this->data['ticket_properties'] = $form->renderAll();
        $comment_form = $this->createCommentForm($URL_params);
        $this->data['comment_form'] = $comment_form->renderAll();
        $reply_form = $this->createReplyForm($URL_params);
        $this->data['reply_form'] = $reply_form->renderAll();
        $this->data['replyes'] = $this->fetchTicketMessages($URL_params);
        $this->data['another_tickets'] = $this->util->getAllUserTickets($ticket['user_id']);
        bdump($this);
        bdump($form, "form");
    }

    protected function createPropertiesForm($ticket, $resources, $types, $supporters, $URL_params) {
        $supp = $this->prepareSupporters($supporters, ($ticket['ticket_level'] > 3 ? false : true));
        $hidden_type = array();
        $hidden_levels = array();
        if (User::getInstance()->getAdminLevel() < 3) {
            $hidden_levels = array(4);
            $hidden_status = array("archived", "waiting");
        }
        if (User::getInstance()->getAdminLevel() < 5) {
            $hidden_levels[] = 5;
        }
        $form = new FormFactory("properties");
        $form->setForm_class("form-inline-md")->setAction("tickets/" . $URL_params[0] . "/change");
        $form->createSelect("resource", null)->
                setOptions($resources, "res_id", "res_name")->
                setSelected($ticket['resource_id'])->
                setPrepend('<i class="fas fa-globe-europe"></i>')
                ->setMargin("")
                ->setOnChange("change(this, 'resource')");
        $form->createSelect("type", null)->
                setOptions(TypesUtils::gI()->getAll(), "type_id", "type_name")->
                setSelected($ticket['type_id'])->
                setPrepend('<i class="fas fa-question-circle"></i>')->
                setVissible($types, "type_id", "type_name")
                ->setMargin("")
                ->setOnChange("change(this, 'type')");
        $form->createSelect("supporter", null)->
                setOptions($supp['supporters'])->
                setSelected($ticket['admin_id'])
                ->setDisabled($supp['disabled'])->
                setHidden($supp['hidden'])->
                setPrepend('<i class="fas fa-user-tie"></i>')
                ->setMargin("")
                ->setOnChange("delegate(this, 'admin')");
        $form->createSelect("level", null)->
                setOptions($this->util->levels)->
                setSelected($ticket['ticket_level'])
                ->setHidden($hidden_levels)->
                setPrepend('<i class="fas fa-level-up-alt"></i>')
                ->setMargin("")
                ->setOnChange("change(this, 'level')");
        $form->createSelect("status", null)->
                setOptions($this->util->status)->
                setSelected($ticket['ticket_status'])
                ->setHidden($hidden_status)->
                setPrepend('<i class="fas fa-check-square"></i>')
                ->setMargin("mb-10")
                ->setOnChange("change(this, 'status')");
        return $form;
    }

    protected function prepareSupporters($admins, $hide_not_delegate = true) {
        bdump($admins);
        $disabled = array();
        $hidden = array();
        $supporters = array(-1 => "none");
        foreach ($admins as $key => $value) {
            if ($value['admin_level'] < 0)
                continue;
            $supporters[$value['user_internal_id']] = $value['login_name'];
            if ($value['holiday']) {
                $supporters[$value['user_internal_id']] = $value['login_name'] . ' (holiday)';
                if ($hide_not_delegate) {
                    $disabled[] = $value['user_internal_id'];
                }
            }
            if ($value['disabled_account']) {
                $supporters[$value['user_internal_id']] = $value['login_name'] . ' (disabled)';
                $disabled[] = $value['user_internal_id'];
            }
            if ((!$value['can_delegate'] && $hide_not_delegate) || !$value['index']) {
                $hidden[] = $value['user_internal_id'];
            }
        }
        return array("disabled" => $disabled, "hidden" => $hidden, "supporters" => $supporters);
    }

    public function createCommentForm($URL_params) {
        $form = new FormFactory("add_comment");
        $form->setAction("tickets/" . $URL_params[0] . "/add_comment")
                ->createTextArea("comment", null)->placeholder(Lang::str("Add comment to ticket"))
                ->required();
        $form->createButton("submit", Lang::str("Add message"))
                ->Class("btn btn-sm btn-success float-right my-10")
                ->value(Lang::str("Add comment"));
        return $form;
    }

    public function createReplyForm($URL_params) {
        $form = new FormFactory("ticket_reply");
        $form->setAction("tickets/" . $URL_params[0] . "/add_reply")
                ->createTextArea("message", null)->placeholder(Lang::str("Write reply here!"))
                ->required();
        $form->createCheckBox("greeting", Lang::str("Add greeting"))->value();
        $form->createButton("submit", Lang::str("Send"))
                ->Class("btn btn-sm btn-success float-right")
                ->value(Lang::str("Send"));

        return $form;
    }

    public function fetchTicketMessages($URL_params) {
        $messages = $this->util->getTicketMessages($URL_params[0]);
        $updateColumns = array("who_time");
        $lastInsertId = "record_id";
        foreach ($messages as $key => $value) {
            $messages[$key]['seen_by'] = $this->util->getWhoSeen($value['msg_id']);
            MysqliDb::getInstance()->onDuplicate($updateColumns, $lastInsertId)
                    ->insert('tickets_messages_who_saw',
                            array("msg_id" => $value['msg_id'],
                                "user_id" => User::getUserId(),
                                "who_time" => date("Y-m-d H:i:s")));
        }


        //$messages = ArrayUtils::makeKeyArray($messages, "msg_created");
        //krsort($messages);
        return $messages;
    }

    public function resource_rights($ticket) {
        $prefix = "res_" . $ticket['res_code'] . "_";
        $rights = User::getInstance();
        return array("view_ip" =>
            $rights->getRuleValue($prefix . "master_see", "IP"),
            "view_who_read" => $rights->getRuleValue($prefix . "master_see", "who_saw"),
            "view_admin_ip" => $rights->getRuleValue($prefix . "master_see", "admin_ip"),
            "ratings" => $rights->getRuleValue($prefix . "master_see", "ratings"));
    }

    public function addReply($URL_params, $ticket) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $msg = '';
        if ($_POST['greeting'] == 'on') {
            $msg .= Lang::str("Hello, <br><br>");
        }
        $msg .= $_POST['message'];
        $msg .= SupportersUtils::gI()->getSignature($ticket['res_id']);
        $this->util->addMessage("admin", $msg, $URL_params[0], User::getUserId());
        $this->util->alterStatus("answered", $URL_params[0]);
        $this->util->addMessage("admin_note", "Status changed to Answered", $URL_params[0], User::getUserId());
        if ($ticket['admin_id'] != User::getUserId()) {
            $this->util->alterAdmin(User::getUserId(), $URL_params[0]);
            $this->util->addMessage("admin_note", "Ticket assigned to " . User::get_instance()->getLoggedUser()['login_name'], $URL_params[0], User::getUserId());
        }
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function addComment($URL_params, $ticket) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $msg = $_POST['comment'];
        $this->util->addMessage("comment", $msg, $URL_params[0], User::getUserId());
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function Change($URL_params, $ticket) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        if ($_POST['change'] == 'resource') {
            $this->util->alterResource($_POST['resource'], $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Resource changed to " . $_POST['resource'], $ticket['ticket_id'], User::getUserId());
        } else if ($_POST['change'] == 'level') {
            $this->util->alterLevel($_POST['level'], $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket level changed to " . $_POST['level'], $ticket['ticket_id'], User::getUserId());
        } else if ($_POST['change'] == 'type') {
            $this->util->alterType($_POST['type'], $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket type changed to " . $_POST['type'], $ticket['ticket_id'], User::getUserId());
        } else if ($_POST['change'] == 'status') {
            $this->util->alterStatus($_POST['status'], $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket status changed to " . $_POST['status'], $ticket['ticket_id'], User::getUserId());
        } else {
            $this->redirectToError("Wrong change!", 501);
        }
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function selfAssign($URL_params, $ticket) {
        $this->util->alterAdmin(User::getUserId(), $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket assigned to " . User::get_instance()->getLoggedUser()['login_name'], $URL_params[0], User::getUserId());
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function delegate($URL_params, $ticket) {
        if ($_POST['change'] == "head") {
            $this->util->alterAdmin(null, $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket assigned to none", $URL_params[0], User::getUserId());
            $this->util->alterLevel(4, $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket level changed to head", $ticket['ticket_id'], User::getUserId());
            $this->util->alterStatus("open", $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket status changed to Open", $ticket['ticket_id'], User::getUserId());
        } else if ($_POST['change'] == "dev") {
            $this->util->alterAdmin(null, $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket assigned to none", $URL_params[0], User::getUserId());
            $this->util->alterLevel(5, $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket level changed to 5", $ticket['ticket_id'], User::getUserId());
            $this->util->alterStatus("open", $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket status changed to Open", $ticket['ticket_id'], User::getUserId());
        } elseif ($_POST['change'] == "admin") {
            $this->util->alterAdmin($_POST['admin'], $ticket['ticket_id']);
            $this->util->addMessage("admin_note", "Ticket assigned to " .
                    ($_POST['admin'] > 0 ? User::get_instance()->getOneUser($_POST['admin'])['login_name'] : "none")
                    , $URL_params[0], User::getUserId());
        }
        if (isset($_POST['comment'])) {
            $this->util->addMessage("comment", $_POST['comment'], $ticket['ticket_id'], User::getUserId());
        }
        $this->redirect("tickets/open");
    }

    public function prevent_close($URL_params, $ticket) {
        $this->util->alterCanClose("0", $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket no longer can be closed by user", $URL_params[0], User::getUserId());
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function allow_close($URL_params, $ticket) {
        $this->util->alterCanClose("1", $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket can now be closed by user", $URL_params[0], User::getUserId());
        $this->redirect("tickets/" . $URL_params[0]);
    }

    public function snooze($URL_params, $ticket) {
        $this->util->alterStatus("pending", $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket status changed to Pending", $URL_params[0], User::getUserId());
        $this->util->alterSnooze(time() + $_POST['snooze'], $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket snoozed until " . date("Y-m-d H:i:s", time() + $_POST['snooze']), $URL_params[0], User::getUserId());
        $this->redirect("tickets/open");
    }

    public function unsnooze($URL_params, $ticket) {
        $this->util->alterStatus("open", $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket status changed to open", $URL_params[0], User::getUserId());
        $this->util->alterSnooze(null, $ticket['ticket_id']);
        $this->util->addMessage("admin_note", "Ticket no longer snoozed", $URL_params[0], User::getUserId());
        $this->redirect("tickets/" . $URL_params[0]);
    }

}
