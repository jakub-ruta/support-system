<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginControler
 *
 * @author jakub
 */
class DashBoardControler extends Controler {

    /**
     *
     * @var class ResourcesUtils
     */
    protected $utils;

    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, false, 0, false);

        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->view = "admin_dashBoard";
        if (RightsUtils::gI()->getOneTrueWithSuffix("_admin", "index")) {
            $this->getAsAdmin($URL_params);
        }

        NotificationsUtils::getInstance()->addNotification("Test", "test contentu");
        NotificationsUtils::getInstance()->addNotification("Test", "test contentu", "just now", "success");
        NotificationsUtils::getInstance()->addNotification("Test", "test contentu", "just now", "primary");
        NotificationsUtils::getInstance()->addNotification("Test", "test contentu", "5 min", "danger", "fas fa-vials", null, "test");
        bdump($this);
    }

    public function prepareResources() {
        $resources = $this->utils->getMyResources();
        foreach ($resources as $key => $value) {
            try {
                $resources[$key]['counts'] = TicketsUtils::gI()->getCounts(User::getUserId(), $value['res_id']);
            } catch (RightException $exc) {
                unset($resources[$key]);
            }
        }
        return $resources;
    }

    public function getAsAdmin($URL_params) {
        $this->utils = ResourcesUtils::gI();
        $resources = $this->prepareResources();
        $this->data['resources'] = ArrayUtils::makeKeyCategoryArray($resources, "group_code");
    }

}
