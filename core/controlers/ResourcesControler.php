<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResourcesControler
 *
 * @author jakub
 */
class ResourcesControler extends TableControlerClass {

    /**
     *
     * @var class ResourcesUtils extends Utils_new
     */
    protected $utils;
    protected $right = "global_resources";
    protected $path = "resources";
    protected $oneView = "resource";

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("global_resources", "view", $URL_params, true, 2, false);
        $this->registerTemplate("template_new", "resources");
        $this->utils = ResourcesUtils::gI();

        $this->registerOneFunc("getTypes");
        $this->registerOneFunc("getDepartments");
        $this->registerOneFunc("getGroups");


        if (is_numeric($URL_params[0]) && $URL_params[1] == "remove_departement" && is_numeric($URL_params[2])) {
            $this->RemoveDepartment($URL_params);
        } elseif (is_numeric($URL_params[0]) && $URL_params[1] == "add_departement") {
            $this->AddDepartment($URL_params);
        } elseif (is_numeric($URL_params[0]) && $URL_params[1] == "change_group") {
            $this->changeGroup($URL_params);
        }

        $this->useRouter($URL_params);
        bdump($this->data);
    }

    public function getTypes($URL_params) {
        $this->data["types"] = TypesUtils::gI()->getByResource($URL_params[0]);
    }

    public function getGroups($URL_params) {
        $this->data["group"] = GroupsUtils::gI()->getByResource($URL_params[0]);
        $this->data['groups'] = GroupsUtils::gI()->getAll();
    }

    public function getDepartments($URL_params) {
        $this->data["departments"] = DepartmentsUtils::gI()->getAll();
        $this->data['assigned_departments'] = $this->utils->getAssigned($URL_params[0]);
    }

    public function AddDepartment($URL_params) {
        CSRFUtils::gI()->checkCSRF($_POST['csrf']);
        $this->utils->addDepartment($URL_params[0], $_POST['departments']);
        $this->addMessage("Resource has been added to department", "success");
        $this->redirect("resources/" . $URL_params[0]);
    }

    public function RemoveDepartment($URL_params) {
        $this->utils->removeDepartment($URL_params[0], $URL_params[2]);
        $this->addMessage("Rsource has been removed from department", "success");
        $this->redirect("resources/" . $URL_params[0]);
    }

    public function changeGroup($URL_params) {
        GroupsUtils::gI()->change($URL_params[0], $_POST['group']);
        $this->addMessage("Rsource group has been changed", "success");
        $this->redirect("resources/" . $URL_params[0]);
    }

}
