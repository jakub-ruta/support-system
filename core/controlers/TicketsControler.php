<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketsControler
 *
 * @author jakub
 */
class TicketsControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 1, true);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (!RightsUtils::gI()->getOneTrueWithSuffix("_admin", "index")) {
            $this->redirectToError("You do not have right to view tickets as admin", 401);
        }
        $next = null;
        if (is_numeric($URL_params[0])) {
            $next = new TicketsAdmin_One();
        } elseif ($URL_params[0] == "select-resource") {
            $this->selectResource($URL_params);
        } else if (isset($URL_params[0])) {
            $next = new TicketsAdmin_Overview();
        } else {
            $this->redirect("tickets/open");
        }
        try {
            $next->execute($URL_params);
        } catch (WrongResourceException $exc) {
            $this->wrongResource($exc->requested, $URL_params);
            return;
        }
        $this->data = array_merge_recursive($this->data, $next->data);
        $this->hasView = $next->hasView;
        $this->view = $next->view;
    }

    public function selectResource($URL_params) {
        if (isset($_POST['csrf'])) {
            CSRFUtils::gI()->checkCSRF($_POST['csrf']);
            $new_res = $_POST['new_res'];
        } else {
            $new_res = $URL_params[1];
        }
        try {
            ResourcesUtils::gI()->setResource($new_res);
            $this->addMessage("Resource has been changed!", "success");
            $this->redirect("tickets/open");
        } catch (RightException $exc) {
            $this->redirectToError($exc->getMessage(), 401);
        }
    }

    public function wrongResource($id, $URL_params) {
        $this->view = "admin_ticket_wrong_resource";
        $form = new FormFactory("change_res");
        $form->setAction("tickets/" . $URL_params[0]);
        $form->createHidden("new_res", $id);
        $form->createButton("change", Lang::str("Change resource"))->Class("form-control my-10 btn btn-success");
        $this->data['form'] = $form->renderAll();
        if ($form->isSend()) {
            try {
                ResourcesUtils::gI()->setResource($form->getValues()['new_res']);
                $this->addMessage("Resource has been changed!", "success");
                $this->redirect("tickets/" . $URL_params[0]);
            } catch (RightException $exc) {
                $this->redirectToError($exc->getMessage(), 401);
            }
        }
    }

}
