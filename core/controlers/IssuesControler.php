<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IssuesControler
 *
 * @author jakub
 */
class IssuesControler extends TableControlerClass {

    protected $utils;
    protected $right = null;
    protected $path = "issues";
    protected $oneView = "issue";

//put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 2, true);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (!DepartmentsUtils::gI()->isUserAdminAnywhere(1)) {
            $this->redirectToError("You are not member of any department", 401);
        }
        if (!RightsUtils::gI()->getOneTrueWithSuffix("_master_see", "issues")) {
            $this->redirectToError("You do not have right to view issues", 401);
        }
        $this->utils = IssuesUtils::gI();
        if (is_numeric($URL_params[0])) {
            $this->oneRouter($URL_params);
        }
        $this->registerOneFunc("oneData");
        $this->useRouter($URL_params);
        bdump($this);
    }

    public function oneRouter($URL_params) {
        if ($URL_params[1] == "assign_type") {
            CSRFUtils::gI()->checkCSRF($_POST['csrf']);
            $this->utils->addType($URL_params[0], $_POST['type']);
            $this->addMessage("Type hass been added to issue");
            $this->redirect("issues/" . $URL_params[0]);
        } else if ($URL_params[1] == "remove_type") {
            $this->utils->removeType($URL_params[0], $URL_params[2]);
            $this->addMessage("Type hass been removed to issue");
            $this->redirect("issues/" . $URL_params[0]);
        } else if ($URL_params[1] == "edit_text") {
            $this->utils->editText($URL_params[0], $_POST);
            $this->addMessage("Text has been updated");
            $this->redirect("issues/" . $URL_params[0]);
        } else if ($URL_params[1] == "add_lang") {
            $this->utils->addText($URL_params[0], $_POST);
            $this->addMessage("Text has been added");
            $this->redirect("issues/" . $URL_params[0]);
        }
    }

    public function All($URL_params) {
        $this->view = "issues";
        $this->data['new_issue_form'] = $this->newIssueForm()->renderAll();
        $this->data['issues'] = $this->utils->getVissible();
    }

    public function oneData($URL_params) {
        $texts = $this->utils->getTexts($URL_params[0]);
        $this->data['form_new_type'] = $this->createAssignNewTypeForm($URL_params, $this->data['issue'])
                ->renderAll();
        $this->data['form_new_lang'] = $this->createNewLangForm($URL_params)->renderAll();
        $forms = array();
        foreach ($texts as $key => $value) {
            $form = new FormFactory("text_edit_" . $value['text_id']);
            $form->setAction("issues/" . $URL_params[0] . "/edit_text");
            $form->createHidden("text_id", $value['text_id']);
            $form->createSelect("lang", null)
                    ->setOptions(Lang::getLangs())
                    ->setHidden(Lang::getLangs())
                    ->value($value['issue_lang'])
                    ->required();
            $form->createTextArea("text", null)
                    ->value($value['issue_text'])
                    ->required();
            $form->createButton("save", "Save");
            $forms[] = $form;
        }
        $this->data['texts'] = $forms;
    }

    public function new() {
        $res = ResourcesUtils::gI()->getOne($_POST['resource']);
        if (!User::getInstance()->getRuleValue("res_" . $res['res_code'] . "_master_see", "issues")) {
            $this->redirectToError("You do not have right to create issue!", 401);
        }

        $this->utils->createNew($res['res_id']);
        $this->addMessage("Issue has been updated!", "success");
        $this->redirect("issues/" . $URL_params[0]);
    }

    public function edit($URL_params) {
        $res_id = IssuesUtils::gI()->getOne($URL_params[0]);
        $res = ResourcesUtils::gI()->getOne($res_id['res_id']);
        if (!User::getInstance()->getRuleValue("res_" . $res['res_code'] . "_master_see", "issues")) {
            $this->redirectToError("You do not have right to edit this issue!", 401);
        }

        $this->utils->updateOne($URL_params[0], $_POST);
        $this->addMessage("Issue has been updated!", "success");
        $this->redirect("issues/" . $URL_params[0]);
    }

    public function createIssueForm($URL_params) {
        $issue = $this->utils->getIssue($URL_params[0]);
        $res = $this->utils->getResources();
        $res_all = ResourcesUtils::gI()->getAll();
    }

    public function createAssignNewTypeForm($URL_params, $issue) {
        $types_assigned = json_decode($issue['issues_types'], true);
        $types = TypesUtils::gI()->getByResource($this->data['issue']['res_id']);
        $form = new FormFactory("new_type");
        $form->setAction("issues/" . $URL_params[0] . "/assign_type");
        $form->createSelect("type", null)
                ->setOptions($types, "type_code", "type_name")
                ->setHidden($types_assigned);
        $form->createButton("add", "Add");
        return $form;
    }

    public function createNewLangForm($URL_params) {
        $form = new FormFactory("new_type");
        $form->setAction("issues/" . $URL_params[0] . "/add_lang");
        $form->createSelect("lang", null)
                ->setOptions(Lang::getLangs());
        $form->createButton("add", "Create translation");
        return $form;
    }

    public function newIssueForm() {
        $form = new FormFactory("new_issue");
        $form->setAction("issues/new")
                ->setForm_class("form form-inline float-right");
        $form->createSelect("resource", null)
                ->setOptions(ResourcesUtils::gI()->getMyResources("_master_see", "issues"), "res_id", "res_name");
        $form->createButton("add", "Create Issue")->Class("form-control btn btn-success btn-sm");
        return $form;
    }

}
