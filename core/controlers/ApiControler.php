<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiControler
 *
 * @author jakub
 */
class ApiControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->hasView = false;
        ini_set('display_errors', 0);
        header('Content-type: application/json');
        switch (array_shift($URL_params)) {

            default :
                $this->returnNotImplemented();
                break;
        }


        return;
    }

    public function returnNotImplemented() {
        header("HTTP/1.0 404 Not Found");
        echo (json_encode(array("status" => "Not Implemented")));
        return;
    }

    public function returnUnathorized($err = "") {
        header("HTTP/1.0 401 Unauthorized");
        echo (json_encode(array("status" => "Unauthorized", "error" => $err)));
        return;
    }

    public function returnSuccess() {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success")));
        return;
    }

    public function returnData($data, $warnings = null, $errors = null) {
        header("HTTP/1.0 200");
        echo (json_encode(array("status" => "success", "errors" => $errors, "warnings" => $warnings,
            "data" => $data)));
        return;
    }

}
