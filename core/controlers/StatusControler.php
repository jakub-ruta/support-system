<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusControler
 *
 * @author jakub
 */
class StatusControler extends Controler {

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("global_status", "view", $URL_params, true, 4, false);
        NotificationsUtils::getInstance()->addNotification("Test", crypt("Jakube1232"));
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        $this->data['msg'] = Lang::str("Everything looks fully operational");
        $this->data['color'] = "success";
        $this->view = "status";
    }

}
