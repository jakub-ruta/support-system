<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TypesControler
 *
 * @author jakub
 */
class TypesControler extends TableControlerClass {

    /**
     *
     * @var class TypesUtils extends Utils_new
     */
    protected $utils;
    protected $right = "global_types";
    protected $path = "types";
    protected $oneView = "type";

    //put your code here
    public function execute($URL_params) {
        $this->initWithRule("global_types", "view", $URL_params, true, 2, false);
        $this->registerTemplate("template_new", "types");
        $this->utils = TypesUtils::gI();
        if ($URL_params[0] == 'repair_rights') {
            $this->repairRights($URL_params);
        }
        $this->useRouter($URL_params);
    }

    public function repairRights($URL_params) {
        $this->utils->repairRights();
        $this->addMessage("Rights has been checked and repaired", "success");
        $this->redirect("resources");
    }

}
