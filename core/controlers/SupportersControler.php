<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SupportersControler
 *
 * @author jakub
 */
class SupportersControler extends Controler {

    private $util;

//put your code here
    public function execute($URL_params) {
        $this->initWithRule(null, null, $URL_params, true, 2, true);
        $this->defaultTemplate = false;
        $this->Template = "template_new";
        if (!DepartmentsUtils::gI()->isUserAdminAnywhere(1)) {
            $this->redirectToError("You are not head of any department", 401);
        }
        if (!RightsUtils::gI()->getOneTrueWithSuffix("_master_see", "supporters")) {
            $this->redirectToError("You do not have right to view supporters", 401);
        }
        $this->util = SupportersUtils::gI();
        $this->router($URL_params);
        bdump($this);
    }

    public function router($URL_params) {
        if (is_numeric($URL_params[0])) {
            $this->one($URL_params);
        } else {
            $this->overview($URL_params);
        }
    }

    public function overview($URL_params) {
        $this->view = "supporters";
        $this->data['supporters'] = $this->util->getEditable();
    }

    public function one($URL_params) {
        $this->addMessage("Use HTML tags to format signature");
        if ($URL_params[1] == "edit-signature") {
            $this->edit($URL_params);
        }
        $this->view = "supporter";
        $this->data['selected_user'] = User::getOneUser($URL_params[0]);
        $this->data['signatures'] = ArrayUtils::makeKeyArray($this->util->getAllForUser($URL_params[0]),
                        "res_id");
        $this->data['resources'] = ResourcesUtils::gI()->getAll();
    }

    public function edit($URL_params) {
        $res = ResourcesUtils::gI()->getOne($URL_params[2]);
        if (!User::getInstance()->getRuleValue("res_" . $res['res_code'] . "_master_see", "supporters")) {
            $this->redirectToError("You do not have right to update signature in this resource!", 401);
        }
        $text = trim($_POST['signature']);
        $this->util->edit($URL_params[2], $URL_params[0], $text);
        $this->addMessage("Signature has been updated!", "success");
        $this->redirect("supporters/" . $URL_params[0]);
    }

}
